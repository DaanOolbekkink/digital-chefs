import { Input } from '@mui/material';
import { Box } from '@mui/system';
import React, { useState } from 'react';
import './App.css';
import { HomeComponent } from './components/HomeComponent';

function App() {
  const [search, setSearch] = useState('');
  return (
    <div className="App">
      <Box sx={{ justifyContent: 'center', height: '70px', display: 'flex' }}>
        <Input value={search} placeholder="Search" sx={{ height: '30px', alignSelf: 'center' }} onChange={(event) => setSearch(event.target.value)} />
      </Box>
      <HomeComponent search={search} />
    </div>
  );
}

export default App;
