export type Image = {
    id: string;
    urls: {
        raw: string,
        full: string,
        regular: string,
        small: string,
        thumb: string,
    }
}