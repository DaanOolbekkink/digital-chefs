import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Box, Grid, ImageList, ImageListItem, Modal, Typography } from '@mui/material';
import { Image } from '../types/photo';
import { AspectRatio } from '@mui/joy';
import './HomeComponent.css'
import InfiniteScroll from 'react-infinite-scroll-component';
import { getAllImages, getImages } from '../services/unsplashService';

export const HomeComponent = ({ search }: { search: string }) => {
    const [allImages, setAllImages] = useState<Image[]>([]);
    const [currentImage, setCurrentImages] = useState<Image>();
    const [page, setPage] = useState(1);
    const [totalImages, setTotalImages] = useState(0);

    const onClose = () => {
        setCurrentImages(undefined);
    }

    const fetchData = async () => {
        const { images, total } = await getImages('', page + 1)
        setPage(page + 1);
        setAllImages([...allImages, ...images]);
        setTotalImages(total)
    }

    useEffect(() => {
        const delayDebounceFn = setTimeout(async () => {
            setPage(1);
            setAllImages([])
            const { images, total } = await getImages(search, page)
            setAllImages(images);
            setTotalImages(total)
        }, 1200)

        return () => clearTimeout(delayDebounceFn)
    }, [search])

    useEffect(() => {
        getAllImages(page).then((result) => {
            setAllImages(result.images)
            setTotalImages(result.total)
        })
    }, [])

    return (
        <>
            {allImages.length > 0 &&
                <InfiniteScroll
                    dataLength={allImages.length}
                    next={fetchData}
                    hasMore={allImages.length < totalImages}
                    loader={<h4>Loading more...</h4>}
                    style={{ padding: 10 }}
                >
                    <Grid
                        container rowSpacing={1}
                        spacing={2}
                    >
                        {allImages.length > 0 && allImages.map((photo, index) => {
                            return (<Grid key={photo.id} item xs={4} md={3} lg={2}>
                                <div className='img-wrapper'>
                                    <AspectRatio ratio="1">
                                        <img
                                            className='grid-img'
                                            src={photo.urls.small}
                                            loading="lazy"
                                            style={{ objectFit: 'cover', borderRadius: 10 }}
                                            onClick={() => setCurrentImages(photo)}
                                        />
                                    </AspectRatio>
                                </div>
                            </Grid>)
                        })}
                    </Grid>
                </InfiniteScroll>
            }
            <Modal
                open={!!currentImage}
                onClose={onClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style} onClick={onClose}>
                    <img
                        onClick={() => { }}
                        src={currentImage?.urls.regular}
                        alt={currentImage?.id}
                        style={{ objectFit: 'contain', width: '100%', height: '100%' }}
                    />
                </Box>
            </Modal>
        </>
    );
}

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '90%',
    height: '80%',
};

