import { Image } from '../types/photo';

const url = 'https://api.unsplash.com'
const key = 'FFmO4sD128u6CJkx75EUcQYgtzaONvU69-BmntgcmCg';

export const getImages = async (search: string, page: number): Promise<{ images: Image[], total: number }> => {
    if (search) {
        return await searchAllImages(search, page);
    } else {
        return await getAllImages(page);
    }
}

export const getAllImages = async (page: number): Promise<{ images: Image[], total: number }> => {
    const response = await fetch(`${url}/photos/?client_id=${key}&per_page=20&page=${page}`);
    const data = await response.json();

    return { images: data, total: parseInt(response.headers.get('X-Total') ?? '0') }
}

const searchAllImages = async (search: string, page: number): Promise<{ images: Image[], total: number }> => {
    const response = await fetch(`${url}/search/photos/?client_id=${key}&per_page=20&query=${search}&page=${page}`);
    const data = (await response.json());

    return { images: data.results, total: data.total }
}